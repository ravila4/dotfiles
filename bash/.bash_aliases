# Bash Aliases and Config
# =======================

# PATH
# ----
export PATH="~/.local/bin":$PATH

# Command preferences
# -------------------
# Set cli edit mode to vi
#set -o vi
export EDITOR="vim"
alias ls="ls --color -h --group-directories-first"
alias rm="trash-put"
alias icat="kitty +kitten icat"
# rlwraps
alias sftp="rlwrap sftp"
alias vmd="rlwrap vmd"
# w3m bookmarks
alias ddg="w3m duckduckgo.com"
alias hn="w3m news.ycombinator.com"
# common folders
alias notes="cd ~/Documents/Notes"
alias ravilabio="cd ~/Projects/ravilabio"
alias mychem="cd ~/Projects/mychem.info"
alias mydisease="cd ~/Projects/mydisease.info"
alias mygeneset="cd ~/Projects/mygeneset.info"
# flatpak apps
#alias code="flatpak run com.visualstudio.code"
alias calibre="flatpak run com.calibre_ebook.calibre"
alias meld="flatpak run org.gnome.meld"
alias blender="flatpak run org.blender.Blender"
alias inkscape="flatpak run org.inkscape.Inkscape"
# docker containers
alias powershell="docker run --rm -v /home/ravila/:/home/ravila/:Z -it mcr.microsoft.com/powershell"

# Functions
# ---------
function csview()
{
    column -s, -t < $1 | less -#2 -N -S
}

function tsview()
{
    column -s\t -t < $1 | less -#2 -N -S
}

tunnel(){
  target=$1
  if [[ "$1" == *:* ]]; then
      host=`echo $1 |cut -d ':' -f1`
      port=`echo $1 |cut -d ':' -f2`
  else
      host='localhost'
      port=$1
  fi

  echo "ssh -L $port:$host:$port scripps"
  ssh -L $port:$host:$port scripps
}

alias ssh-mychem="ssh -L 27080:su03:27080 scripps"
alias ssh-mygeneset="ssh -L 19480:su12:19480 scripps"
alias ssh-cerebro="ssh -L 9000:su07:9000 scripps"

# setup ccze related functions
if type "ccze" > /dev/null 2>&1; then
    # if ccze is available
    function cless { cat $* | ccze -A -o nolookups | less -R; }
    function scless { sudo cat $* | ccze -A -o nolookups | less -R; }
    function ctail { tail -f $* | ccze -A -o nolookups; }
    function sctail { sudo tail -f $* | ccze -A -o nolookups; }
    function jfc { sudo journalctl -f -u $* | ccze -A -o nolookups; }
fi

# setup pushd/dirs/popd related aliases in bash
if [ $0 = "bash" ]; then
    alias d='dirs -v | head -10'
    alias 1='cd $(dirs -l +1)'
    alias 2='cd $(dirs -l +2)'
    alias 3='cd $(dirs -l +3)'
    alias 4='cd $(dirs -l +4)'
    alias 5='cd $(dirs -l +5)'

    # set default dirs stack
    #  example:
    #  WORKING_DIRS=(/nvme/mydisease ~/venv/src/biothings ~/mydisease.info/src)
    WORKING_DIRS=(~/Projects ~/data)
    set_dirs(){
        if [ -x "$WORKING_DIRS" ]; then
            dirs -c    # empty dir stack
            for w_dir in ${WORKING_DIRS[@]}; do
                pushd -n $w_dir > /dev/null
            done
        fi
    }
    # require WORKING_DIRS to be set first
    set_dirs
fi

# API Keys
# --------
# Spotify API
export SPOTIPY_CLIENT_ID='5739c6c8807b413281c8963851d88af7'
export SPOTIPY_CLIENT_SECRET='e4e1eefce215461086454c49069cb8a6'
# Last.fm API
export LASTFM_KEY='137465300856b0ecd99ed0372adc2d45'
export LASTFM_SECRET='54f02ffb4e8362c82192b141c8f5cb3b'
export LASTFM_USER='Aeneas42'
export LASTFM_HASH='bdbc7725c746e67c25ea6a095faa93c1'
# Google API
export GOOGLE_API_KEY='AIzaSyCIM4EzNqi1in22f4Z3Ru3iYvLaY8tc3bo'
